#!/bin/python3
from PIL import Image
import sys
import os
import pathlib


current_dir = pathlib.Path(__file__).parent.resolve()


def save_and_export(size: tuple, name: str, path: str = current_dir, export_dir: str = 'export') -> None:
    resized_image = image.resize(size)

    output_path = os.path.join(path, export_dir)
    size_output_path = os.path.join(output_path, f"{size[0]}x{size[1]}")
    apps_dir = os.path.join(size_output_path, 'apps')
    os.makedirs(apps_dir)
    resized_image.save(f"{apps_dir}/{name}.png")


if(__name__ == '__main__'):
    args = sys.argv.copy()
    file = args[1]
    name = file.replace('.png', '')

    image = Image.open(file)
    save_and_export((16,16), name)
    save_and_export((24,24), name)
    save_and_export((32,32), name)
    save_and_export((48,48), name)
    save_and_export((64,64), name)
    save_and_export((96,96), name)
    save_and_export((128,128), name)
    save_and_export((256,256), name)